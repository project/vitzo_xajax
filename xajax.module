<?php
/**
 * XAJAX Module for Drupal
 * 
 * Some common XAJAX functions and easy implementation functions for Drupal are provided
 * More information about XAJAX can be obtained at http://www.xajaxproject.org/
 * 
 * @copyright GPL (http://www.gnu.org/copyleft/gpl.html)
 * @author Vitzo Limited (www.vitzo.com)
 */

define('DS','/'); // Directory seperator
include_once("inc/extended.php");

/**
 * Implementation of hook_help().
 */
 function xajax_help($section)
 {
   switch ($section) {
     case 'admin/settings/xajax':
       return '<p>'.t("Configure XAJAX settings.").'</p>';
   }
 }
 
 /**
 * Implementation of hook_menu().
 */
function xajax_menu() {
  
  $items['admin/settings/xajax'] = array(
    'title' => t('XAJAX Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xajax_settings'),
    'access arguments' => array('xajax settings'),
  );
  $items['admin/settings/xajax/settings'] = array(
    'title' => t('Configure settings'),
    'weight' => 0,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function xajax_perm() {
  return array('xajax settings');
}

/**
 * Form builder; Configure XAJAX module.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function xajax_settings() {
  $form['xajax_js_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Full URI to XAJAX include directory'),
    '#default_value' => xajax_get_uri(),
    '#size' => 100,
    '#description' => t('The complete URI/URL to the include directory/folder is needed to reference in the JavaScript which is dynamically inserted in the HTML headers.')
  );

  $form['#validate'] = array('xajax_settings_validate');
  
  return system_settings_form($form);
}

/**
 * Validate the submitted form values
 */
function xajax_settings_validate($form, &$form_state) {
  if (strstr($form_state['values']['xajax_js_uri'],"http://") === false) {
    form_set_error('xajax_js_uri', t("%value is not a full URI. Please include http://", array('%value' => $form_state['values']['xajax_js_uri'])));
  }
}

/**
 * Prepare generic XAJAX framework for use in Drupal
 * If you need to define custom functions, copy this code and pass an array to xajaxInit() 
 * 
 * @param string Debug mode enabled or disabled
 */
function xajax_generic($debug) {
  $xajax = xajaxInit();
  if ($debug) $xajax->configure('debug',true);
  drupal_set_html_head($xajax->getJavascript(xajax_get_uri()));
  $xajax->processRequest();
}

/**
 * The URI to use as parameter when calling $xajax->getJavascript()
 */
function xajax_get_uri() {
  return variable_get('xajax_js_uri', rtrim("http://".$_SERVER["HTTP_HOST"],"/")."/".drupal_get_path('module', 'xajax')."/inc/");
}

?>