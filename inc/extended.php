<?php
/**
 * Extended XAJAX functions for Drupal modules
 * 
 * @copyright GPL (http://www.gnu.org/copyleft/gpl.html)
 * @author Ken De Volder for Vitzo Limited (www.vitzo.com)
 */

include_once("xajax_core".DS."xajax.inc.php");

/**
 * Register XAJAX functions
 * 
 * @param string Use * to register all common functions
 * @param array Register the function names in the array
 * @return object XAJAX object
 */
function xajaxInit($register = "*")
{
	$xajax = new xajax();
	
	if ($register == "*")
	{
		$xajax->register(XAJAX_FUNCTION,"array2DB");
	}
	else
	{
		foreach ($register as $name)
		{
			$xajax->register(XAJAX_FUNCTION,$name);
		}
	}
	
	$xajax->configure('debug',VERBOSE);
	
	return $xajax;
}

/**
 * XAJAX function which updates or inserts rows in the active database
 * 
 * @param string Name of the table to update
 * @param string Fields as name=value+name2=value2
 * @param string SQL definition including WHERE keyword
 */
function array2DB($table,$fields,$where=null)
{
	$temp = explode("+",$fields);
	for ($i=0; $i<count($temp); $i++)
	{
		$temp2 = explode("=",$temp[$i],2);
		
		// Insert into definitive array as key=>value
		$queryArray[$temp2[0]] = mysql_escape_string($temp2[1]); 
	}
	$result = db_persist($table,$queryArray,$where);
	
	$xajax = new xajaxResponse();
	
	if (!$result) $xajax->alert("DB could not be updated!\r\n".db_error());
	
	return $xajax;
}